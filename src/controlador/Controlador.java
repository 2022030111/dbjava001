/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author Gabiela
 */

import modelo.*;
import Vista.jifProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;


public class Controlador implements ActionListener{
    private dbProducto db;
    private jifProductos vista;
    private boolean esActualizar;
    private int idProducto;
    
    //constructores

    public Controlador(jifProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;
        
        //Escuchar el evento clic de los siguientes botones 
        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    
    //metodos helpers
    
    public void iniciarVista(){
        vista.setTitle("Productos");
        vista.setVisible(true);
        vista.resize(750, 700);
        
    
    try{
        this.ActualizarTabla(db.lista());
    }catch(Exception e){
        JOptionPane.showMessageDialog(vista,"Surgio un error" + e.getMessage());
    }
    
    }
    
    public String convertirAñoMesDia(Date fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    
    public void convertirStringDate(String fecha){
        try{
            //Convertir la cadena de texto a un objeto Date
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date= dateFormat.parse(fecha);
            vista.jdcFecha.setDate(date);
        } catch(ParseException e){
            System.err.print(e.getMessage());
        }
    }
    
    public void limpiar(){
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.jdcFecha.setDate(new Date());
    }
    
    public void cerrar(){
        int res = JOptionPane.showConfirmDialog(vista,"Desea cerrar el Sistema", "Productos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        if(res == JOptionPane.YES_OPTION){
            vista.dispose();
        }
    }
    
    public void habilitar(){
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
    }
    
    public void deshabilitar(){
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);
    }
    
    public boolean validar(){
        boolean exito = true; 
        
        if(vista.txtCodigo.getText().equals("") || vista.txtNombre.getText().equals("") || vista.txtPrecio.getText().equals("")) exito = false;
        
        return exito;
}
    public void ActualizarTabla(ArrayList<Productos> arr){
        String campos[]= {"idProducto","Codigo","Nombre","Precio","Fecha"};
                String[][] datos = new String[arr.size()][5];
                int renglon = 0;
                for(Productos registro : arr){
                    datos[renglon][0] = String.valueOf(registro.getIdProductos());
                    datos[renglon][1] = registro.getCodigo();
                    datos[renglon][2] = registro.getNombre();
                    
                    datos[renglon][3] = String.valueOf(registro.getPrecio());
                    datos[renglon][4] = registro.getFecha();
                    renglon ++;
                }
                DefaultTableModel tb = new DefaultTableModel(datos,campos);
                vista.tblProductos.setModel(tb);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource()==vista.btnLimpiar) this.limpiar();
        if(ae.getSource()==vista.btnCancelar) {this.limpiar(); this.deshabilitar();}
        if(ae.getSource()==vista.btnCerrar) this.cerrar();
        if(ae.getSource()==vista.btnNuevo) {this.limpiar();this.habilitar();this.esActualizar=false; vista.txtCodigo.requestFocus();}
        
        
        if(ae.getSource()==vista.btnBuscar){
            Productos pro = new Productos();
            //Validar
            if(vista.txtCodigo.equals("")){
                JOptionPane.showMessageDialog(vista,"Favor de Capturar el Código");
                vista.txtCodigo.requestFocus();
            }
            else{
                try{
                pro = (Productos) db.buscar(vista.txtCodigo.getText());
                
                if(pro.getIdProductos()!=0){
                    //Aleluya si lo enocntró
                    
                    //mostrar infromación 
                    vista.txtNombre.setText(pro.getNombre());
                    vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                    
                    this.convertirStringDate(pro.getFecha());
                    
                    vista.btnGuardar.setEnabled(true);
                    vista.btnDeshabilitar.setEnabled(true);
                    this.esActualizar = true;
                    this.idProducto = pro.getIdProductos();
                }else JOptionPane.showMessageDialog(vista,"No se encontro producto con codigo" + vista.txtCodigo.getText());
                
                }catch(Exception e){
                    JOptionPane.showMessageDialog(vista,"Surgio error al buscar" + e.getMessage());
                }
            }
        }
        
        if(ae.getSource()==vista.btnDeshabilitar){
            Productos pro = new Productos();
            pro.setIdProductos(this.idProducto);
            int res = JOptionPane.showConfirmDialog(vista, "¿Está seguro que desea deshabilitar?","Productos",JOptionPane.YES_OPTION,JOptionPane.QUESTION_MESSAGE); 
            if(res==JOptionPane.YES_OPTION){
                try {
                db.deshabilitar(pro);
                this.limpiar();
                this.deshabilitar();
                this.ActualizarTabla(db.lista());
                //Actualizar la tabla 
            } catch(Exception e) {
                JOptionPane.showMessageDialog(vista, "Surgió un error al deshabilitar el producto: " + e.getMessage());
            }
            }   
        }  
        if(ae.getSource()==vista.btnGuardar){
            //hacer el objeto de la clase producto
            Productos pro = new Productos();
            
           
            if(this.validar()==true){
                //todo bn
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setFecha(this.convertirAñoMesDia(vista.jdcFecha.getDate()));
                try{
                    if(this.esActualizar == false){
                        
                db.insertar(pro);
                JOptionPane.showMessageDialog(vista,"Se agregó con exito el Producto con codigo" + vista.txtCodigo);
                this.limpiar();
                this.deshabilitar();
                 this.ActualizarTabla(db.lista());
                
                //actualizar tabla
                } else{
                        pro.setIdProductos(idProducto);
                        //Se actualizará
                        db.actualizar(pro);
                        JOptionPane.showMessageDialog(vista,"Se actualizo con exito el Producto con codigo" + vista.txtCodigo.getText());
                        this.limpiar();
                        this.deshabilitar();
                         this.ActualizarTabla(db.lista());
                        //actualizar tabla
                    }
                }catch(Exception e){
                    JOptionPane.showMessageDialog(vista,"Surgio un error al insertar Producto"+ e.getMessage());
                }
            }else JOptionPane.showMessageDialog(vista,"Falto informacion");
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
    
}